#About ConnectMe Framework
The ConnectMe Framework (CMF) is a product of Salzburg Research and is developed within the funded project ConnectMe.
It builds an application on to of the Linked Media Framework and bundles Apache Marmotta, Apache Stanbol and Apache Solr.
#Configuration
CMF provides several components; some of them must be configured individually and depend on the use case. The components are:

1. Video Resource Creator
2. Stanbol Enhancement Engine
3. LSI Enhancement Engine

##Video Resource Creator
The Video Resource Creator is able to create video resources following the Ontology for Media Resources. Users or
application import videos and metadata from MediaRSS feeds as well as creating new video resource by providing video
source and basic metadata. Both webservices return basic properties of the video resource in json format:

<pre>
{
  title: "..."
  uri: "..."
  source: [
    "...ogv",
    "...mp4"
  ]
}
</pre>
###Create Video Resource via MediaRSS
For this methode users or applications need information about the video id as well as the channel id and the id of the
origin. The Webservice call is a HTTP GET and must follow this scheme:

`GET {BASE}/video/resource/rss?id=...&origin=...&channel=...`

The parameters are:

* id: a string
* channel: a string
* origin: enum (yoovis|psmedia) (At the moment only 'yoovis' is supported)
* keywords: a comma seperated list of strings (OPTIONAL)

###Create/Updating Video Resource via source url
To add video resource from video resourvce url, you can use the following webservice:

`GET {BASE}/video/resource/url?url=...&title=...&description=...&keywords=...&location=...`

The parameters are:

* url: an http url that points to the video resource (including format extension)
* title: a string (OPTIONAL)
* description: a string (OPTIONAL)
* keywords: a comma seperated list of strings (OPTIONAL)
* location: a LD location URI (OPTIONAL)

###Create Video Resource via with url and subtitle file
To add video resource from video resourvce url, you can use the following webservice:

`POST {BASE}/video/resource/srt?url=...&title=...  @file.srt`

The parameters are:

* url: an http url that points to the video resource (including format extension)
* title: a string (OPTIONAL)
* @file.srt: the srt file in the body if the request. (http://en.wikipedia.org/wiki/SubRip)

With curl, command can look like this:
`curl -X POST --data-binary @tbbt.sample.en.sr --data-binary "@tbbt.sample.en.srt" "http://localhost:8080/video/resource/srt?title=Hello+World&url=http://ecample.org/video/123"`

##Stanbol Enhancement Engine
Stanbol is an enhancent engine that allows the linkage of normal text to linked data entities.
###Stanbol Configuration
Stanbol Configuration is quite easy with the ConnectMe Framework but requires several steps. First of all, a referenced
Site must be installed. This means, that you must import an index into the Stanbol storage and make it accessible to
enhacement engines. As a second step, you must define a enhancement chain (that includes several enhancement engines) and
link it to the index.

The whole configuration is well described on the [the LMF description](https://code.google.com/p/lmf/wiki/ModuleStanbol").
###Enhancer Configuration
The configuration of the enhancer can be done via integrating a stanbol function into an LDPath program. In CMF this
can be done by using a webserivce or by loading up a program via user interface in the Enhancer Module.

A program, that triggers Stanbol to link all MediaResource to LD concepts regarding their local subjects can look like this:

    @prefix myont: <http://localhost:8080/LMF/resource/myont/>
    @prefix ma: <http://www.w3.org/ns/ma-ont#>
    @prefix lode: <http://linkedevents.org/ontology/>
    @prefix dc: <http://purl.org/dc/terms/>

    @filter rdf:type is ma:MediaResource ;

    myont:extractedSubjects = myont:hasEvent [rdf:type is lode:Event] / dc:description[@en] :: stanbol:engine(name="dbpedia");

##Other Enhancement Engines
In addition to stanbol, there are other enhancement engines, that can be used in a similar style.
###LSI Enhancement Engine
LSI is a webservice, that returns videos and images based on a given linked data resource. Like Stanbol, it must can be triggered by LDPath Function.
A program, that triggers LSI to find related resource for all MediaResource regarding their subjects can look like this:

    @prefix myont: <http://localhost:8080/LMF/resource/myont/>
    @prefix ma : <http://www.w3.org/ns/ma-ont#>

    @filter rdf:type is ma:MediaResource ;

    myont:relatedImages = . :: service:lsi(lod="<http://purl.org/dc/terms/subject>",mediaType=image,limit=3,context="<http://www.w3.org/ns/ma-ont#hasRelatedLocation>",ner=true);

The parameters are:

* lod := (LDPath Program)a LDPath description, which resources should be used as contexts. Prefixes are not yet supported.
* mediaType := (Enum: image | video | all) which type of resources are returned
* limit := (positiveInteger) the limit of resources that are returned, 10 by default
* ner := (boolean) if ner is active or not, default false
* context := (LDPath Program) describes, how (and if) a context is connected to the (Media)Resource, default undefined. Prefixes are not yet supported.

###SRT Enhancement Engine
This engines generates ma:MediaFragments out of srt files. The text is appended to the fragment using dc:description.

    @prefix ma : <http://www.w3.org/ns/ma-ont#> ;

    @filter fn:startsWith(., "http://connectme.primetv.at") ;
      srt = . :: service:srtImport(relation="<http://www.w3.org/ns/ma-ont#hasSubtitling>",locator="<http://www.w3.org/ns/ma-ont#locator>") ;

The parameters are:

* realtion := (LDPath Query)a LDPath query that points to the srt file; ma:hasSubtitling by default; OPTIONAL
* locator := (LDPath Query)a LDPath query that point to the locator(s); ma:locator by default; OPTIONAL

##Reasoner
CMF includes a Reasoning Engine, that can be configure in the regarding module. It is quite useful to clean your enhancemnt data.
Stanbol uses FISE, an ontology for enitity linking. Enhancemnts include many information, that are not necessarily needed in any use case.
The following reasoning program binds all link generated by the stanbol enhancer to the MediaResource with dc:subject relation:

    @prefix myont: <http://localhost:8080/LMF/resource/myont/>
    @prefix dct : <http://purl.org/dc/terms/>
    @prefix fise: <http://fise.iks-project.eu/ontology/>
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>

    ($1 myont:extractedSubjects $2), ($2 rdf:type fise:EntityAnnotation), ($2 fise:entity-reference $3) -> ($1 dct:subject $3).

#Sample Data
Import this sample data (or modiefied versions) to play with the enhancers.

    @prefix myont: <http://localhost:8080/cmf/resource/myont/> .
    @prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
    @prefix ma: <http://www.w3.org/ns/ma-ont#> .
    @prefix dc: <http://purl.org/dc/terms/> .
    @prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

    <http://upload.wikimedia.org/wikipedia/commons/2/23/President_Obama_nominates_Elena_Kagan_for_Supreme_Court>
        rdf:type ma:MediaResource ;
        ma:locator "http://upload.wikimedia.org/wikipedia/commons/2/23/President_Obama_nominates_Elena_Kagan_for_Supreme_Court.ogv"^^xsd:anyURI ;
        dc:title "Nomination of Elena Kagan"@en ;
        dc:description "Barack Obama, the president of the United States of America nominates Elena Kagan for Supreme Court"@en ;
        dc:subject <http://dbpedia.org/resource/Barack_Obama> ;
        ma:hasRelatedLocation <http://dbpedia.org/resource/Washington_DC> .

You can find the sample file under /data/sample.ttl.