<%@ page import="org.apache.marmotta.platform.core.api.config.ConfigurationService" %>
<%@ page import="org.apache.marmotta.platform.core.util.CDIContext" %>
<%--
  ~ Copyright (c) 2008-2012 Salzburg Research.
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~     http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<!-- Redirect should never be displayed -->
<%
    ConfigurationService configurationService = CDIContext.getInstance(ConfigurationService.class);
%>
<link href="<%=configurationService.getBaseUri()%>img/icon-small.ico" rel="SHORTCUT ICON">
<meta http-equiv="REFRESH" content='0;url=<%= configurationService.getBaseUri()+configurationService.getStringConfiguration("kiwi.pages.startup") %>'>
</head>
</HTML>
