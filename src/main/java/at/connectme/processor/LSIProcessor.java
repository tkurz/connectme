package at.connectme.processor;

import at.connectme.api.LSIService;
import at.connectme.api.LSIServiceException;
import at.connectme.api.MediaType;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementResult;
import org.apache.marmotta.commons.vocabulary.MA;
import org.apache.marmotta.ldpath.api.backend.RDFBackend;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.openrdf.model.*;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@ApplicationScoped
public class LSIProcessor implements EnhancementProcessor {

    private static String NS_URI = "http://connectme.at/service/";

    @Inject
    private Logger log;

    @Inject
    private LSIService lsiService;

    @Inject
    private LDPathService ldPathService;

    @Inject
    private SesameService sesameService;

    public String getLocalName() {
        return "lsi";
    }

    public String getNamespaceUri() {
        return NS_URI;
    }

    public String getNamespacePrefix() {
        return "service";
    }

    public EnhancementResult transform(RDFBackend<Value> backend, Value value, Map<String, String> configuration) throws IllegalArgumentException {
        MediaType mediaType = null;
        Integer limit = -1;
        boolean ner = false;
        String context_string = null;
        String lod_string = null;
        if (configuration != null && configuration.containsKey("mediaType")) {
            mediaType = MediaType.valueOf(configuration.get("mediaType"));
        }
        if (configuration != null && configuration.containsKey("ner")) {
            ner = Boolean.parseBoolean(configuration.get("ner"));
        }
        if (configuration != null && configuration.containsKey("limit")) {
            limit = Integer.parseInt(configuration.get("limit"));
        }
        if(configuration != null && configuration.containsKey("context")) {
            context_string = configuration.get("context");
        }
        if(configuration != null && configuration.containsKey("lod")) {
            lod_string = configuration.get("lod");
        } else {
           throw new IllegalArgumentException("lod must be defined");
        }

        try {
            //repository may not be null;
            Repository repository = sesameService.getRepository();
            Repository repo = new SailRepository(new MemoryStore());
            repo.initialize();

            Set<Resource> uris = new HashSet<Resource>();

            //get system connection
            try {
                List<URI> lods = getLODs(repository, (URI) value, lod_string);
                //query for context
                URI context = getContext(repository,(URI)value,context_string);

                for (URI lod : lods) {

                        try {
                            lsiService.run(lod, context, mediaType, limit, ner, repo);
                            log.info("process <"+lod+"> with context <"+context+">");
                        } catch(Exception e) {
                            continue;
                        }


                    RepositoryConnection connection = repo.getConnection();
                    connection.begin();
                    try {
                        //get a list of images and videos
                        TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                                "SELECT ?uri WHERE {{?uri <" + RDF.TYPE + "> <" + MA.VideoTrack + ">} " +
                                        "UNION {?uri <" + RDF.TYPE + "> <" + MA.Image + ">}}");
                        TupleQueryResult results = tupleQuery.evaluate();

                        //iterate and store
                        while (results.hasNext()) {
                            Value v = results.next().getValue("uri");
                            if (v instanceof URI) {
                                uris.add((URI) v);
                            } else if (v instanceof BNode) {
                                uris.add((BNode) v);
                            }
                        }
                    } catch (Exception e) {
                        log.error("Some exception during lsi enhament: " + e.getMessage());
                    } finally {
                        connection.commit();
                        connection.close();
                    }

                }

                return new EnhancementResult(uris, repo);

            } catch (LSIServiceException e) {
                log.error("lsi service did not work properly: " + e.getMessage());
                return new EnhancementResult(uris, repo);
            } catch (Exception e) {
                log.error("getting sesame connection was not possible");
                throw new RuntimeException("getting sesame connection was not possible");
            }
        } catch (Exception e) {
            throw new RuntimeException("getting sesame connection was not possible");
        }
    }

    private List<URI> getLODs(Repository repository, URI uri, String lod_string) throws Exception {
        if(lod_string == null) return null;

        List<URI> uris= new ArrayList<URI>();
        RepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            connection.begin();

            Collection<Value> result = ldPathService.pathQuery(uri, lod_string, Collections.EMPTY_MAP);

            Iterator<Value> it = result.iterator();
            while(it.hasNext()) {
                Value v = it.next();
                if(v instanceof URI) {
                    uris.add((URI)v);
                }
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return uris;
    }

    private URI getContext(Repository repository, URI uri, String context_string) throws Exception {
        if(context_string == null) return null;

        URI contextURI=null;
        RepositoryConnection connection = null;
        try {
            connection = repository.getConnection();
            connection.begin();

            Collection<Value> result = ldPathService.pathQuery(uri, context_string, Collections.EMPTY_MAP);

            if(!result.isEmpty()) {
                Value v = result.iterator().next();
                if(v instanceof URI) {
                    contextURI = (URI) v;
                }
            }
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
        return contextURI;
    }
}
