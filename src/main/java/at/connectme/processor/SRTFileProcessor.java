package at.connectme.processor;

import at.connectme.importer.SRTImporter;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementProcessor;
import at.newmedialab.lmf.enhancer.model.ldpath.EnhancementResult;
import org.apache.marmotta.commons.vocabulary.MA;
import org.apache.marmotta.ldpath.api.backend.RDFBackend;
import org.apache.marmotta.platform.core.api.importer.ImportService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.api.user.UserService;
import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.openrdf.model.*;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.net.URL;
import java.util.*;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
@ApplicationScoped
public class SRTFileProcessor implements EnhancementProcessor {

    @Inject
    private Logger log;

    @Inject
    SesameService sesameService;

    @Inject
    UserService userService;

    @Inject
    SRTImporter srtImporter;

    @Inject
    LDPathService ldPathService;

    @Override
    public String getNamespaceUri() {
        return "http://connectme.at/service/";
    }

    @Override
    public String getNamespacePrefix() {
        return "service";
    }

    @Override
    public String getLocalName() {
        return "srtImport";
    }

    @Override
    public EnhancementResult transform(RDFBackend<Value> valueRDFBackend, Value value, Map<String, String> configuration) throws IllegalArgumentException {

        String relation = "<"+MA.hasSubtitling.stringValue()+">";
        String locator = "<"+MA.locator.stringValue()+">";

        if (configuration != null && configuration.containsKey("relation")) {
            relation = configuration.get("relation");
        }

        if (configuration != null && configuration.containsKey("locator")) {
            locator = configuration.get("locator");
        }

        try {

            Set<Resource> uris = new HashSet<>();
            Repository repo = new SailRepository(new MemoryStore());
            repo.initialize();

            //import
            try {
                Collection<Value> results = ldPathService.pathQuery(value,relation, Collections.EMPTY_MAP);
                Collection<Value> locators = ldPathService.pathQuery(value,locator, Collections.EMPTY_MAP);
                for (Value v : results) {
                    if((locators.isEmpty())) {
                        srtImporter.importData(new URL(v.stringValue()), "application/x-subrip", userService.getCurrentUser(), (URI) value);
                    } else {
                        List<String> locs = new ArrayList<>();
                        for(Value l : locators) {
                            locs.add(l.stringValue());
                        }
                        srtImporter.importData(new URL(v.stringValue()), "application/x-subrip", userService.getCurrentUser(), (URI) value,locs);
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("cannot import srt file");
            }

            //return an empty result
            return new EnhancementResult(uris, repo);

        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
}
