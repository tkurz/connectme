package at.connectme.importer;

import org.apache.commons.io.IOUtils;
import org.openrdf.model.vocabulary.RDF;
import org.slf4j.Logger;
import org.apache.marmotta.commons.vocabulary.MA;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.content.ContentService;
import org.apache.marmotta.platform.core.api.importer.Importer;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.exception.WritingNotSupportedException;
import org.apache.marmotta.platform.core.exception.io.MarmottaImportException;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.DC;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import javax.inject.Inject;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class SRTImporter implements Importer {

    @Inject
    private Logger log;

    @Inject
    SesameService sesameService;

    @Inject
    ConfigurationService configurationService;

    @Inject
    ContentService contentService;

    private static final String nl = "\r\n";
    private static final String sp = "[ \t]*";
    private static Pattern pattern =Pattern.compile("(?s)(\\d+)" + sp + nl + "(\\d{1,2}):(\\d\\d):(\\d\\d),(\\d\\d\\d)" + sp + "-->"+ sp + "(\\d\\d):(\\d\\d):(\\d\\d),(\\d\\d\\d)" + sp + "(X1:\\d.*?)??" + nl + "(.*?)" + nl + nl);

    private static final Set<String> format = new HashSet<String>(){{
       add("application/x-subrip");
    }};

    @Override
    public String getName() {
        return "Subtitle Importer";
    }

    @Override
    public String getDescription() {
        return "Allows to import subtitles in the SubRip file format. Context MUST be the related video";
    }

    @Override
    public Set<String> getAcceptTypes() {
        return format;
    }

    /**
     *
     * @param url
     * @param s
     * @param user
     * @param uri to this uri the subtitle is bound
     * @return
     * @throws MarmottaImportException
     */
    @Override
    public int importData(URL url, String s, Resource user, URI uri) throws MarmottaImportException {
        return importData(url,s,user,uri,Collections.EMPTY_LIST);
    }

    @Override
    public int importData(InputStream inputStream, String s, Resource user, URI uri) throws MarmottaImportException {
        return importData(inputStream,s,user,uri,Collections.EMPTY_LIST);
    }

    @Override
    public int importData(Reader reader, String s, Resource user, URI uri) throws MarmottaImportException {
        return importData(reader,s,user,uri,Collections.EMPTY_LIST);
    }

    public int importData(Reader reader, String s, Resource user, URI uri, List<String> locators) throws MarmottaImportException {
        try {
            return parseInput(IOUtils.toString(reader),uri,locators);
        } catch (IOException e) {
            throw new MarmottaImportException(e);
        }
    }

    public int importData(URL url, String s, Resource user, URI uri, List<String> locators) throws MarmottaImportException {
        try {
            InputStream in = null;
            try {
                in = url.openStream();
                return importData(in,s,user,uri,locators);
            } catch (IOException e) {
                log.error(e.getMessage());
                throw new MarmottaImportException(e.getMessage());
            } finally {
                if(in != null) in.close();
            }
        } catch(IOException e) {
            log.error(e.getMessage());
            throw new MarmottaImportException(e.getMessage());
        }
    }

    public int importData(InputStream inputStream, String s, Resource user, URI uri, List<String> locators) throws MarmottaImportException {
        try {
            return parseInput(IOUtils.toString(inputStream),uri, locators);
        } catch (IOException e) {
            throw new MarmottaImportException(e);
        }
    }

    private int parseInput(String s, URI uri, List<String> locators) {

        RepositoryConnection connection = null;
        try {

            connection = sesameService.getConnection();

            ValueFactory factory = connection.getValueFactory();
            connection.begin();

            //Matcher matcher = pattern.matcher("1\\n00:00:00,084 --> 00:00:02,000\\n<i>Previously on </i>The Big Bang Theory...\\n\\n2\\n00:00:02,170 --> 00:00:05,585\\nI am going to the Arctic Circle\\nwith Leonard, Wolowitz and Koothrappali.\\n\\n3\\n00:00:05,841 --> 00:00:07,677\\n- For three months?\\n- Yes.\\n\\n4\\n00:00:08,145 --> 00:00:11,079\\nWhat did you mean when you said\\nyou were going to miss me?\\n\\n5\\n00:00:13,333 --> 00:00:15,508\\nIt means I wish you weren't going.\\n\\n6\\n00:00:30,172 --> 00:00:31,446\\nThank God we're home.\\n\\n7\\n00:00:31,616 --> 00:00:35,248\\nI can't believe we spent three months\\nin that frozen hell.\\n\\n8\\n00:00:35,787 --> 00:00:39,287\\nIt was like a snowy nightmare\\nfrom which there was no awakening.\\n\\n9\\n00:00:39,916 --> 00:00:42,335\\nI don't know what Arctic expedition\\nyou guys were on,\\n\\n10\\n00:00:42,460 --> 00:00:44,542\\nbut I thought it was\\na hoot and a half.\\n\\n11\\n00:00:47,962 --> 00:00:48,962\\nHi, Mom.\\n\\n12\\n00:00:50,213 --> 00:00:53,134\\nI told you I'd call you when I got home.\\nI'm not home yet.\\n\\n13\\n00:00:54,711 --> 00:00:56,157\\nAll right, I'm home.\\n\\n14\\n00:00:57,497 --> 00:01:00,285\\nThe Arctic expedition\\nwas a remarkable success.\\n\\n15\\n00:01:00,410 --> 00:01:02,800\\nI'm all but certain\\nthere's a Nobel Prize in my future.\\n\\n16\\n00:01:03,164 --> 00:01:05,649\\nActually, I shouldn't say that.\\nI'm entirely certain.\\n\\n17\\n00:01:07,861 --> 00:01:11,111\\nNo, Mother, I could not feel\\nyour church group praying for my safety.\\n\\n18\\n00:01:12,413 --> 00:01:14,462\\nThe fact that I'm home safe\\ndoesn't prove it worked.\\n\\n19\\n00:01:14,587 --> 00:01:16,648\\nThat logic is post hoc\\nergo propter hoc.\\n\\n20\\n00:01:18,523 --> 00:01:21,245\\nNo, I'm not sassing you\\nin Eskimo talk.\\n\\n21\\n00:01:22,987 --> 00:01:24,523\\nI'm gonna let Penny know we're back.\\n\\n22\\n00:01:24,648 --> 00:01:27,388\\nMother, I have to go.\\nLove you. Bye.\\n\\n23\\n00:01:29,465 --> 00:01:30,791\\nHello, old friend.\\n\\n24\\n00:01:39,718 --> 00:01:41,054\\nDaddy's home.\\n\\n25\\n00:01:44,284 --> 00:01:45,603\\nLeonard, you're back.\\n\\n26\\n00:01:45,774 --> 00:01:47,482\\nI just stopped by to say...\\n\\n27\\n00:01:53,275 --> 00:01:54,070\\nSo, hi.\\n\\n28\\n00:02:03,400 --> 00:02:06,834\\nDamn it, I should have gone over\\nand told her we were back.\\n\\n29\\n00:02:11,805 --> 00:02:14,180\\nYeah, it was\\nfirst-come, first-serve.\\n\\n30\\n00:02:16,428 --> 00:02:19,513\\nEpisode 301:\\nThe Electric Can Opener Fluctuation\\n\\n31\\n00:02:21,798 --> 00:02:23,520\\nSync: Jess, Golgi, Loky34\\n\\n32\\n00:02:36,408 --> 00:02:38,520\\nwww.subbercafe.fr\\n\\n33\\n00:02:41,329 --> 00:02:42,703\\nI just want you both to know,\\n\\n34\\n00:02:42,871 --> 00:02:46,040\\nwhen I publish my findings,\\nI won't forget your contributions.\\n\\n35\\n00:02:46,497 --> 00:02:47,808\\n- Great.\\n- Thanks.\\n\\n36\\n00:02:48,210 --> 00:02:50,501\\nOf course, I can't mention you\\nin my Nobel acceptance speech,\\n\\n37\\n00:02:50,626 --> 00:02:52,429\\nbut when I get around\\nto writing my memoirs,\\n\\n38\\n00:02:52,554 --> 00:02:55,973\\nyou can expect a very effusive footnote\\nand perhaps a signed copy.\\n\\n39\\n00:02:57,683 --> 00:02:58,886\\nWe have to tell him.\\n\\n40\\n00:02:59,054 --> 00:03:00,106\\nTell me what?\\n\\n41\\n00:03:01,500 --> 00:03:03,094\\nDamn his Vulcan hearing.\\n\\n42\\n00:03:04,712 --> 00:03:06,936\\nYou fellows are planning a party\\nfor me, aren't you?\\n\\n43\\n00:03:07,935 --> 00:03:09,563\\nOkay, Sheldon, sit down.\\n\\n44\\n00:03:10,449 --> 00:03:12,999\\nIf there's going to be a theme,\\nI should let you know\\n\\n45\\n00:03:13,124 --> 00:03:15,528\\nthat I don't care for luau, toga\\nor \"under the sea.\"\\n\\n46\\n00:03:17,890 --> 00:03:19,949\\nYeah, we'll keep that in mind.\\nLook...\\n\\n47\\n00:03:20,372 --> 00:03:24,125\\nWe need to talk to you about something\\nthat happened at the North Pole.\\n\\n48\\n00:03:24,495 --> 00:03:26,612\\nIf this is about the night\\nthe heat went out,\\n\\n49\\n00:03:26,737 --> 00:03:29,143\\nthere's nothing to be\\nembarrassed about.\\n\\n50\\n00:03:30,686 --> 00:03:33,615\\n- It's not about that.\\n- We agreed to never speak of it again.\\n\\n51\\n00:03:34,256 --> 00:03:35,881\\nSo we slept together naked.\\n\\n52\\n00:03:38,936 --> 00:03:41,762\\nIt was only to keep our core body\\ntemperatures from plummeting.\\n\\n53\\n00:03:43,310 --> 00:03:44,852\\nHe's speaking about it.\\n\\n54\\n00:03:45,601 --> 00:03:47,649\\nFor me, it was a bonding moment.\\n\\n55\\n00:03:51,938 --> 00:03:54,608\\nSheldon, you remember\\nthe first few weeks\\n\\n56\\n00:03:54,776 --> 00:03:56,811\\nwe were looking\\nfor magnetic monopoles\\n\\n57\\n00:03:56,936 --> 00:03:58,350\\nand not finding anything\\n\\n58\\n00:03:58,475 --> 00:04:01,956\\nand you were acting\\nlike an obnoxious, giant dictator?\\n\\n59\\n00:04:02,541 --> 00:04:03,993\\nWe were gonna be gentle with him.\\n\\n60\\n00:04:04,308 --> 00:04:06,461\\nThat's why I added the \"tator.\"\\n\\n61\\n00:04:11,126 --> 00:04:13,500\\nAnd when we finally\\ngot our first positive data,\\n\\n62\\n00:04:13,625 --> 00:04:14,962\\nyou were so happy.\\n\\n63\\n00:04:16,125 --> 00:04:19,192\\nIn the world of emoticons,\\nI was colon, capital \"D.\"\\n\\n64\\n00:04:25,757 --> 00:04:28,753\\nWell, in actuality,\\nwhat your equipment detected\\n\\n65\\n00:04:28,878 --> 00:04:32,621\\nwasn't so much evidence\\nof paradigm-shifting monopoles\\n\\n66\\n00:04:32,750 --> 00:04:33,881\\nas it was...\\n\\n67\\n00:04:34,939 --> 00:04:38,068\\nstatic from the electric can opener\\nwe were turning on and off.\\n\\n68\\n00:04:40,614 --> 00:04:42,579\\nHe just went colon, capital \"O.\"\\n\\n69\\n00:04:46,063 --> 00:04:48,412\\n- You tampered with my experiment?\\n- We had to.\\n\\n70\\n00:04:48,580 --> 00:04:52,025\\nIt was the only way to keep you\\nfrom being such a huge Dickensian.\\n\\n71\\n00:04:53,624 --> 00:04:55,673\\nYou see that? I add the \"ensian. \"\\n\\n72\\n00:04:57,664 --> 00:04:59,314\\nDid Leonard know about this?\\n\\n73\\n00:04:59,439 --> 00:05:02,092\\nLeonard's my best friend in the world.\\nSurely Leonard didn't know.\\n\\n74\\n00:05:02,260 --> 00:05:03,385\\nIt was his idea.\\n\\n75\\n00:05:03,553 --> 00:05:06,012\\nOf course it was.\\nThe whole plan reeks of Leonard.\\n\\n76\\n00:05:07,766 --> 00:05:09,627\\n- I missed you so much.\\n- I missed you, too.\\n\\n77\\n00:05:09,752 --> 00:05:11,783\\nI couldn't think of anyone else\\nwhile you were gone.\\n\\n78\\n00:05:11,908 --> 00:05:12,908\\nMe, neither.\\n\\n79\\n00:05:14,395 --> 00:05:16,926\\nExcept for one night\\nwhen the heat went out.\\n\\n80\\n00:05:18,557 --> 00:05:20,055\\nLong story. Don't ask.\\n\\n81\\n00:05:27,189 --> 00:05:29,605\\nDo not make a sound.\\n\\n82\\n00:05:30,732 --> 00:05:33,160\\nWhispering,\\n\"Do not make a sound... \"\\n\\n83\\n00:05:34,672 --> 00:05:35,860\\nis a sound.\\n\\n84\\n00:05:37,881 --> 00:05:39,791\\nDamn his Vulcan hearing.\\n\\n85\\n00:05:40,941 --> 00:05:42,608\\nNot a good time, Sheldon.\\n\\n86\\n00:05:47,084 --> 00:05:48,347\\nOh, this is ridiculous.\\n\\n87\\n00:05:50,183 --> 00:05:51,225\\n- What?\\n- Hello.\\n\\n88\\n00:05:52,019 --> 00:05:55,396\\nI realize you're currently at the mercy\\nof your primitive biological urges,\\n\\n89\\n00:05:55,564 --> 00:05:58,137\\nbut as you have an entire lifetime\\nof poor decisions ahead of you,\\n\\n90\\n00:05:58,262 --> 00:05:59,898\\nmay I interrupt this one?\\n\\n91\\n00:06:01,365 --> 00:06:02,987\\nIt's great to see you too.\\nCome on in.\\n\\n92\\n00:06:03,982 --> 00:06:07,064\\nWolowitz has informed me\\nof your grand deception.\\n\\n93\\n00:06:07,314 --> 00:06:09,034\\nDo you have anything\\nto say for yourself?\\n\\n94\\n00:06:09,202 --> 00:06:11,397\\nYes, I feel terrible about it.\\n\\n95\\n00:06:11,522 --> 00:06:14,157\\nI will never forgive myself,\\nI don't expect you to, either,\\n\\n96\\n00:06:14,282 --> 00:06:16,692\\nand I would really appreciate it\\nif you would leave me with Penny\\n\\n97\\n00:06:16,817 --> 00:06:19,364\\nfor a session of self-criticism\\nand repentance.\\n\\n98\\n00:06:23,449 --> 00:06:25,467\\nCan someone please tell me\\nwhat's going on here?\\n\\n99\\n00:06:25,710 --> 00:06:27,900\\nWhat's going on is\\nI was led to believe\\n\\n100\\n00:06:28,025 --> 00:06:30,180\\nI was making\\ngroundbreaking strides in science,\\n\\n101\\n00:06:30,348 --> 00:06:31,597\\nwhen in fact, I was being\\n\\n102\\n00:06:31,767 --> 00:06:34,351\\nfed false data at the hands\\nof Wolowitz, Koothrappali\\n\\n103\\n00:06:34,476 --> 00:06:36,456\\nand your furry little boy toy.\\n\\n104\\n00:06:38,646 --> 00:06:39,773\\nIs that true?\\n\\n105\\n00:06:39,942 --> 00:06:42,443\\nIt was the only way\\nto make him happy.\\n\\n106\\n00:06:42,611 --> 00:06:43,944\\nMake him happy?\\n\\n107\\n00:06:44,112 --> 00:06:46,447\\nWhen he wasn't happy,\\nwe wanted to kill him.\\n\\n108\\n00:06:48,074 --> 00:06:49,725\\nThere was even a plan.\\n\\n109\\n00:06:51,161 --> 00:06:53,327\\nWe were going to throw\\nhis Kindle outside,\\n\\n110\\n00:06:53,454 --> 00:06:55,039\\nwhen he went to get it, lock the door\\n\\n111\\n00:06:55,207 --> 00:06:56,749\\nand let him freeze to death.\\n\\n112\\n00:06:58,068 --> 00:07:00,209\\nThat seems like a bit\\nof an overreaction.\\n\\n113\\n00:07:00,629 --> 00:07:02,773\\nThe overreaction\\nwas the plan to tie your limbs\\n\\n114\\n00:07:02,898 --> 00:07:05,299\\nto four different sled dog teams\\nand yell, \"Mush. \"\\n\\n115\\n00:07:08,401 --> 00:07:09,834\\nWe kept the original data.\\n\\n116\\n00:07:09,959 --> 00:07:12,068\\nYou can still publish\\nthe actual results.\\n\\n117\\n00:07:12,474 --> 00:07:15,225\\nYes, but the actual results\\nare unsuccessful\\n\\n118\\n00:07:15,393 --> 00:07:18,068\\nand I've already sent an e-mail\\nto everyone at the university\\n\\n119\\n00:07:18,193 --> 00:07:20,048\\nexplaining that I have\\nconfirmed string theory\\n\\n120\\n00:07:20,173 --> 00:07:22,524\\nand forever changed\\nman's understanding of the universe.\\n\\n121\\n00:07:22,692 --> 00:07:25,087\\nAw, see, yeah, you probably\\nshouldn't have done that.\\n\\n122\\n00:07:26,822 --> 00:07:29,907\\nSo write another e-mail.\\nSet the record straight. No big deal.\\n\\n123\\n00:07:31,878 --> 00:07:33,327\\nYou're right, Leonard.\\n\\n124\\n00:07:33,496 --> 00:07:34,996\\nIt's not a big deal.\\n\\n125\\n00:07:35,253 --> 00:07:37,829\\nAll you did was lie to me,\\ndestroy my dream\\n\\n126\\n00:07:37,954 --> 00:07:41,004\\nand humiliate me\\nin front of the whole university.\\n\\n127\\n00:07:43,041 --> 00:07:45,399\\nThat, FYI, was sarcasm.\\n\\n128\\n00:07:47,131 --> 00:07:49,531\\nI, in fact,\\nbelieve it is a big deal.\\n\\n129\\n00:07:51,756 --> 00:07:53,132\\nThe poor thing.\\n\\n130\\n00:07:53,257 --> 00:07:55,349\\n- Yeah, I feel terrible.\\n- Wait. Wait.\\n\\n131\\n00:07:56,007 --> 00:07:57,643\\nAren't you going to go talk to him?\\n\\n132\\n00:07:57,811 --> 00:08:00,854\\nHe'll be fine. The guy's a trouper.\\nCome here.\\n\\n133\\n00:08:01,696 --> 00:08:04,381\\nYou're right, you shouldn't talk to him.\\nI will.\\n\\n134\\n00:08:07,029 --> 00:08:08,847\\nMan, I cannot catch a break.\\n\\n135\\n00:08:17,693 --> 00:08:19,193\\nDo you want to talk?\\n\\n136\\n00:08:19,324 --> 00:08:20,374\\nAbout what?\\n\\n137\\n00:08:21,012 --> 00:08:22,835\\nBeing betrayed by my friends?\\n\\n138\\n00:08:23,199 --> 00:08:25,763\\nSpending three months\\nat the North Pole for nothing?\\n\\n139\\n00:08:27,636 --> 00:08:30,236\\nAnd I didn't even get\\nto go to Comic-Con!\\n\\n140\\n00:08:33,188 --> 00:08:34,262\\nOh, hon...\\n\\n141\\n00:08:41,261 --> 00:08:44,148\\n<i>Soft kitty, warm kitty</i>\\n\\n142\\n00:08:44,316 --> 00:08:46,817\\nThat's for when I'm sick.\\nSad is not sick.\\n\\n143\\n00:08:47,520 --> 00:08:49,528\\nSorry.\\nI don't know your sad song.\\n\\n144\\n00:08:49,696 --> 00:08:51,934\\nI don't have a sad song.\\nI'm not a child.\\n\\n145\\n00:08:54,591 --> 00:08:57,486\\nYou know, I do understand\\nwhat you're going through.\\n\\n146\\n00:08:57,611 --> 00:08:58,412\\nReally?\\n\\n147\\n00:08:58,741 --> 00:09:01,812\\nDid you just have the Nobel Prize\\nin waitressing stolen from you?\\n\\n148\\n00:09:03,206 --> 00:09:06,639\\nWell, no, but when I was a senior\\nin high school,\\n\\n149\\n00:09:06,764 --> 00:09:09,464\\none of my friends heard\\nI was gonna be named head cheerleader.\\n\\n150\\n00:09:09,640 --> 00:09:10,859\\nI was so excited.\\n\\n151\\n00:09:10,984 --> 00:09:13,403\\nMy mom even made me\\na celebration pie.\\n\\n152\\n00:09:14,012 --> 00:09:16,891\\nThen they named stupid\\nValerie Mossbacher head cheerleader.\\n\\n153\\n00:09:17,016 --> 00:09:18,316\\nBig ol' slutbag.\\n\\n154\\n00:09:20,326 --> 00:09:21,526\\nAre you saying\\n\\n155\\n00:09:22,000 --> 00:09:23,771\\nthat you think a \"celebration pie\"\\n\\n156\\n00:09:23,896 --> 00:09:27,051\\nis even remotely\\ncomparable to a Nobel Prize?\\n\\n157\\n00:09:28,033 --> 00:09:29,583\\nThey're pretty tasty.\\n\\n158\\n00:09:31,842 --> 00:09:34,860\\nAnd on a different,\\nbut not unrelated topic,\\n\\n159\\n00:09:34,985 --> 00:09:37,736\\nbased on your current efforts\\nto buoy my spirits,\\n\\n160\\n00:09:37,861 --> 00:09:39,798\\ndo you truly believe\\nthat you were ever fit\\n\\n161\\n00:09:39,923 --> 00:09:41,889\\nto be a cheerleader?\\n\\n162\\n00:09:47,676 --> 00:09:50,296\\nLook, Sheldon, I just don't think\\nthat the guys and Leonard\\n\\n163\\n00:09:50,480 --> 00:09:52,289\\nreally meant to hurt you.\\n\\n164\\n00:09:52,414 --> 00:09:55,886\\nThey just told an unfortunate lie\\nto deal with a difficult situation.\\n\\n165\\n00:09:56,280 --> 00:09:57,471\\nYou know what it's like?\\n\\n166\\n00:09:57,639 --> 00:10:00,830\\nRemember that scene in the new Star Trek\\nwhen Kirk has to take over the ship,\\n\\n167\\n00:10:01,022 --> 00:10:03,146\\nso he tells Spock all that stuff\\nhe knew wasn't true,\\n\\n168\\n00:10:03,271 --> 00:10:05,971\\nlike saying\\nSpock didn't care his mom died?\\n\\n169\\n00:10:07,147 --> 00:10:09,316\\nI missed Comic-Con\\nand the New Star Trek movie!\\n\\n170\\n00:10:14,207 --> 00:10:16,357\\n- I like the new look.\\n- Thanks.\\n\\n171\\n00:10:20,149 --> 00:10:21,827\\nI call it \"the Clooney.\"\\n\\n172\\n00:10:23,399 --> 00:10:25,999\\nI call it \"the Mario and Luigi,\"\\nbut whatever.\\n\\n173\\n00:10:28,298 --> 00:10:29,586\\nHow's Sheldon doing?\\n\\n174\\n00:10:29,788 --> 00:10:32,514\\nHe came out of his room this morning\\nwearing his Darth Vader helmet\\n\\n175\\n00:10:32,639 --> 00:10:34,531\\nand tried to choke me to death\\nwith the force,\\n\\n176\\n00:10:34,656 --> 00:10:36,527\\nso I'd say, \"a little better.\"\\n\\n177\\n00:10:38,036 --> 00:10:41,723\\nIf I may abruptly change the subject,\\ndid you and Penny finally... You know.\\n\\n178\\n00:10:42,086 --> 00:10:45,277\\nPersonally, I don't care,\\nbut my genitals wanted me to ask.\\n\\n179\\n00:10:46,564 --> 00:10:49,522\\nTell your genitals what I do with Penny\\nis none of their business.\\n\\n180\\n00:10:50,907 --> 00:10:52,707\\nHe says they didn't do it.\\n\\n181\\n00:10:54,697 --> 00:10:56,145\\nSheldon, over here.\\n\\n182\\n00:11:10,913 --> 00:11:12,086\\nWhat are you doing?\\n\\n183\\n00:11:13,376 --> 00:11:15,026\\nI feel bad for the guy.\\n\\n184\\n00:11:18,471 --> 00:11:20,428\\nSheldon, why are you\\nsitting by yourself?\\n\\n185\\n00:11:20,597 --> 00:11:22,315\\nBecause I am without friends.\\n\\n186\\n00:11:22,440 --> 00:11:25,316\\nLike the proverbial cheese,\\nI stand alone.\\n\\n187\\n00:11:26,095 --> 00:11:27,495\\nEven while seated.\\n\\n188\\n00:11:27,968 --> 00:11:29,784\\nCome on. We said we were sorry.\\n\\n189\\n00:11:29,909 --> 00:11:31,719\\nIt's going to take more\\nthan an \"I'm sorry\"\\n\\n190\\n00:11:31,844 --> 00:11:33,782\\nand a store-bought\\napology pie from Penny\\n\\n191\\n00:11:33,907 --> 00:11:36,110\\nto make up\\nfor what you've done to me.\\n\\n192\\n00:11:37,407 --> 00:11:38,829\\nRead your retraction email.\\n\\n193\\n00:11:38,954 --> 00:11:41,077\\nWay to destroy your reputation.\\n\\n194\\n00:11:42,278 --> 00:11:43,087\\nYou see?\\n\\n195\\n00:11:43,212 --> 00:11:45,787\\nPeople have been pointing\\nand laughing at me all morning.\\n\\n196\\n00:11:45,955 --> 00:11:47,255\\nThat's not true.\\n\\n197\\n00:11:47,723 --> 00:11:50,166\\nPeople have been pointing\\nand laughing at you your whole life.\\n\\n198\\n00:11:50,880 --> 00:11:52,085\\nI've had enough.\\n\\n199\\n00:11:52,253 --> 00:11:53,461\\nAttention, everyone.\\n\\n200\\n00:11:53,720 --> 00:11:55,296\\nI'm Dr. Sheldon Cooper.\\n\\n201\\n00:11:55,465 --> 00:11:58,033\\nAs many of you\\nin the physics department might know,\\n\\n202\\n00:11:58,158 --> 00:12:00,599\\nmy career trajectory\\nhas taken a minor detour.\\n\\n203\\n00:12:00,724 --> 00:12:01,824\\nOff a cliff.\\n\\n204\\n00:12:04,141 --> 00:12:05,937\\nMy credibility\\nmay have been damaged...\\n\\n205\\n00:12:06,062 --> 00:12:07,150\\nCompletely wrecked.\\n\\n206\\n00:12:07,275 --> 00:12:10,294\\nBut I would like to remind you\\nthat in science,\\n\\n207\\n00:12:10,419 --> 00:12:11,938\\nthere's no such thing as failure.\\n\\n208\\n00:12:12,385 --> 00:12:13,648\\nThere once was a man\\n\\n209\\n00:12:13,816 --> 00:12:16,557\\nwho referred to his prediction\\nof a cosmological constant\\n\\n210\\n00:12:16,682 --> 00:12:19,350\\nas the single \"biggest blunder\"\\nof his career.\\n\\n211\\n00:12:19,475 --> 00:12:22,323\\nThat man's name was...\\nsurprise, surprise...\\n\\n212\\n00:12:22,621 --> 00:12:23,658\\nAlbert Einstein.\\n\\n213\\n00:12:25,599 --> 00:12:27,501\\nYeah, but research into dark energy\\n\\n214\\n00:12:27,626 --> 00:12:29,475\\nproved that Einstein's\\ncosmological constant\\n\\n215\\n00:12:29,600 --> 00:12:32,221\\nwas actually right all along,\\nso you're still...\\n\\n216\\n00:12:32,346 --> 00:12:33,838\\nsurprise, surprise...\\n\\n217\\n00:12:33,963 --> 00:12:34,963\\nA loser.\\n\\n218\\n00:12:38,222 --> 00:12:39,848\\nYou think you're so clever.\\n\\n219\\n00:12:39,973 --> 00:12:41,088\\nLet me just tell you,\\n\\n220\\n00:12:41,213 --> 00:12:43,442\\nwhile I do not currently\\nhave a scathing retort,\\n\\n221\\n00:12:43,567 --> 00:12:46,124\\nyou check your e-mail\\nperiodically for a doozy.\\n\\n222\\n00:12:51,347 --> 00:12:53,688\\nSo much for our friendship\\nwith Sheldon.\\n\\n223\\n00:12:55,336 --> 00:12:57,525\\nWe'll always have the night\\nthe heat went out.\\n\\n224\\n00:13:08,216 --> 00:13:10,095\\nSince we got interrupted last night,\\n\\n225\\n00:13:10,220 --> 00:13:12,164\\nI didn't have a chance\\nto give you this.\\n\\n226\\n00:13:13,444 --> 00:13:14,894\\nYou shouldn't have.\\n\\n227\\n00:13:17,266 --> 00:13:18,266\\nOh, boy!\\n\\n228\\n00:13:20,518 --> 00:13:21,568\\nWhat is it?\\n\\n229\\n00:13:22,406 --> 00:13:23,756\\nIt's a snowflake.\\n\\n230\\n00:13:23,904 --> 00:13:25,177\\nFrom the North Pole.\\n\\n231\\n00:13:26,464 --> 00:13:27,764\\nAre you serious?\\n\\n232\\n00:13:28,725 --> 00:13:29,605\\nIt's eternal.\\n\\n233\\n00:13:29,730 --> 00:13:32,770\\nI preserved it in a one percent solution\\nof polyvinyl acetal resin.\\n\\n234\\n00:13:38,100 --> 00:13:40,257\\nThat's the most romantic thing\\nanyone's ever said to me\\n\\n235\\n00:13:40,382 --> 00:13:41,847\\nthat I didn't understand.\\n\\n236\\n00:13:43,347 --> 00:13:45,011\\nIt's actually a simple process.\\n\\n237\\n00:13:45,136 --> 00:13:48,072\\nCyanoacrylates are monomers\\nwhich polymerize on...\\n\\n238\\n00:13:52,380 --> 00:13:54,122\\nRed alert, Leonard.\\nSheldon ran away.\\n\\n239\\n00:13:54,293 --> 00:13:55,862\\nMan, I cannot catch a break.\\n\\n240\\n00:13:57,427 --> 00:13:59,086\\nSo, how do you know he ran away?\\n\\n241\\n00:13:59,320 --> 00:14:00,912\\nWell, he's not answering his phone,\\n\\n242\\n00:14:01,037 --> 00:14:03,212\\nhe handed in his\\nresignation at the university\\n\\n243\\n00:14:03,337 --> 00:14:06,385\\nand he sent me a text\\nthat said, \"I'm running away.\"\\n\\n244\\n00:14:07,304 --> 00:14:08,787\\nThanks for letting me know.\\n\\n245\\n00:14:08,912 --> 00:14:10,680\\nAren't you going to do something?\\n\\n246\\n00:14:11,320 --> 00:14:13,725\\nOf course I'm going to do something.\\n\\n247\\n00:14:14,476 --> 00:14:17,351\\nHoward, you check the comic book store.\\nRaj, go to the thai restaurant.\\n\\n248\\n00:14:17,476 --> 00:14:19,644\\nI'll stay here with Penny\\nin her apartment.\\n\\n249\\n00:14:21,451 --> 00:14:22,639\\nOh, damn it.\\n\\n250\\n00:14:23,623 --> 00:14:25,028\\nIt's Sheldon's mother.\\n\\n251\\n00:14:25,297 --> 00:14:27,047\\nA break cannot be caught.\\n\\n252\\n00:14:28,203 --> 00:14:29,324\\nHi, Mrs. Cooper.\\n\\n253\\n00:14:30,358 --> 00:14:32,558\\nHe is?\\nSheldon went home to Texas.\\n\\n254\\n00:14:34,077 --> 00:14:36,027\\nYeah, no, I know he resigned.\\n\\n255\\n00:14:37,946 --> 00:14:40,046\\nI guess it kind of is our fault.\\n\\n256\\n00:14:41,198 --> 00:14:43,743\\nNo, you're right.\\nSomeone needs to come talk to him.\\n\\n257\\n00:14:43,868 --> 00:14:45,712\\nDon't worry, I'll take care of it.\\n\\n258\\n00:14:46,667 --> 00:14:47,667\\nAll right.\\n\\n259\\n00:14:49,034 --> 00:14:50,034\\nNew plan.\\n\\n260\\n00:14:50,166 --> 00:14:53,441\\nHoward, you and Raj go to Texas.\\nI'll stay with Penny in her apartment.\\n\\n261\\n00:14:54,626 --> 00:14:56,017\\nYou're not gonna go with them?\\n\\n262\\n00:14:56,185 --> 00:14:58,700\\nWell, you know,\\nI gave you the snowflake\\n\\n263\\n00:14:58,825 --> 00:15:00,099\\nand we were kissing and...\\n\\n264\\n00:15:00,224 --> 00:15:02,190\\nCome on,\\nI don't want to go to Texas!\\n\\n265\\n00:15:03,420 --> 00:15:06,120\\nOh, right, and I do?\\nMy people already crossed a desert once.\\n\\n266\\n00:15:06,245 --> 00:15:07,295\\nWe're done.\\n\\n267\\n00:15:11,137 --> 00:15:12,659\\nTrust me, you'll be fine. See ya.\\n\\n268\\n00:15:13,266 --> 00:15:14,655\\nWell, wait a second, Leonard.\\n\\n269\\n00:15:14,780 --> 00:15:17,084\\nCome on, how can you not go?\\nHe's your best friend.\\n\\n270\\n00:15:17,209 --> 00:15:19,604\\nYeah, but I already saw him naked.\\nJust come here.\\n\\n271\\n00:15:20,854 --> 00:15:23,104\\nI promise I will be here\\nwhen you get back.\\n\\n272\\n00:15:23,229 --> 00:15:24,713\\nJust go help Sheldon.\\n\\n273\\n00:15:25,605 --> 00:15:26,668\\n- Really?\\n- Yeah.\\n\\n274\\n00:15:26,793 --> 00:15:29,363\\nWe waited a few months.\\nWe can wait a few more days.\\n\\n275\\n00:15:34,418 --> 00:15:35,618\\nMaybe you can.\\n\\n276\\n00:15:38,133 --> 00:15:39,133\\nGo.\\n\\n277\\n00:15:41,378 --> 00:15:43,878\\nBoy, you cannot\\ncatch a break, can you?\\n\\n278\\n00:15:47,575 --> 00:15:49,075\\nHere you go, Shelly.\\n\\n279\\n00:15:49,499 --> 00:15:50,599\\nThanks, Mom.\\n\\n280\\n00:15:52,969 --> 00:15:54,603\\nHold your horses, young man.\\n\\n281\\n00:15:54,728 --> 00:15:56,661\\nHere in Texas,\\nwe pray before we eat.\\n\\n282\\n00:15:56,900 --> 00:15:57,900\\nAw, Mom.\\n\\n283\\n00:15:58,025 --> 00:16:00,775\\nThis is not California,\\nland of the heathen.\\n\\n284\\n00:16:02,543 --> 00:16:03,543\\nGimme.\\n\\n285\\n00:16:07,941 --> 00:16:10,027\\nBy His hand we are all...\\n\\n286\\n00:16:11,293 --> 00:16:12,293\\n... fed.\\n\\n287\\n00:16:12,496 --> 00:16:14,285\\nGive us, Lord, our daily...\\n\\n288\\n00:16:15,041 --> 00:16:16,041\\n... bread.\\n\\n289\\n00:16:16,246 --> 00:16:18,572\\n- Please know that we are truly...\\n- ... grateful.\\n\\n290\\n00:16:18,697 --> 00:16:21,331\\n- For every cup and every...\\n- ... plateful.\\n\\n291\\n00:16:25,762 --> 00:16:27,609\\nNow, that wasn't so hard, was it?\\n\\n292\\n00:16:28,235 --> 00:16:30,009\\nMy objection was based\\non considerations\\n\\n293\\n00:16:30,134 --> 00:16:31,734\\nother than difficulty.\\n\\n294\\n00:16:32,781 --> 00:16:34,584\\nWhatever.\\nJesus still loves you.\\n\\n295\\n00:16:37,796 --> 00:16:40,396\\nThank you for carving a smiley face\\nin my grilled cheese sandwich.\\n\\n296\\n00:16:40,521 --> 00:16:42,507\\nOh, I know\\nhow to take care of my baby.\\n\\n297\\n00:16:43,487 --> 00:16:46,540\\nHis eyes came out a little thin,\\nbut you can just pretend he's Chinese.\\n\\n298\\n00:16:51,820 --> 00:16:53,759\\nSo, do you want to talk about\\n\\n299\\n00:16:53,884 --> 00:16:56,471\\nwhat happened with you\\nand your little friends?\\n\\n300\\n00:16:57,150 --> 00:16:59,391\\n- They're not my friends.\\n- All right.\\n\\n301\\n00:17:02,098 --> 00:17:04,128\\nIf you recall, when you were little,\\n\\n302\\n00:17:04,253 --> 00:17:06,147\\nwe sat right here at this very spot\\n\\n303\\n00:17:06,316 --> 00:17:07,796\\nand we talked about the problems\\n\\n304\\n00:17:07,921 --> 00:17:10,193\\nyou had getting along\\nwith the neighbor kids.\\n\\n305\\n00:17:10,363 --> 00:17:11,445\\nThat was different.\\n\\n306\\n00:17:11,570 --> 00:17:13,225\\nThey were threatened\\nby my intelligence\\n\\n307\\n00:17:13,350 --> 00:17:15,903\\nand too stupid to know\\nthat's why they hated me.\\n\\n308\\n00:17:18,695 --> 00:17:21,212\\nOh baby, they knew very well\\nwhy they hated you.\\n\\n309\\n00:17:33,308 --> 00:17:35,468\\nI can't believe\\nyou bought a red cowboy hat.\\n\\n310\\n00:17:35,857 --> 00:17:38,093\\nHello?\\nI'm wearing a red turtleneck.\\n\\n311\\n00:17:43,035 --> 00:17:44,958\\nPlus, it was the only boys' large\\nthey had.\\n\\n312\\n00:17:47,600 --> 00:17:49,537\\nI'm sorry,\\nthis does not look like Texas.\\n\\n313\\n00:17:49,662 --> 00:17:51,819\\nWhere's the tumbleweeds?\\nWhe's the saloons?\\n\\n314\\n00:17:51,944 --> 00:17:52,944\\nSaloons?\\n\\n315\\n00:17:53,112 --> 00:17:54,929\\nLike in the movies\\nI saw growing up in India.\\n\\n316\\n00:17:55,054 --> 00:17:57,932\\nYou know, <i>Four for Texas</i>,\\n<i>Yellow Rose of Texas.</i>\\n\\n317\\n00:17:58,190 --> 00:18:01,383\\nThis neighborhood is more\\n<i>Texas Chainsaw Massacre</i>.\\n\\n318\\n00:18:03,027 --> 00:18:05,081\\nI was really hoping\\nto see a cattle drive.\\n\\n319\\n00:18:05,252 --> 00:18:06,373\\nWhat can I tell you?\\n\\n320\\n00:18:06,787 --> 00:18:09,707\\nThey probably have steaks on sale\\nat that big-ass Costco over there.\\n\\n321\\n00:18:19,529 --> 00:18:21,684\\nWill you please\\ntake that stupid hat off?\\n\\n322\\n00:18:22,060 --> 00:18:23,558\\nNo, I want to blend in.\\n\\n323\\n00:18:25,458 --> 00:18:27,133\\nTo what? <i>Toy Story?</i>\\n\\n324\\n00:18:30,048 --> 00:18:32,067\\n- Hi, boys.\\n- Howdy, ma'am.\\n\\n325\\n00:18:32,237 --> 00:18:34,519\\nHowdy to you, too.\\nYou got here quick.\\n\\n326\\n00:18:34,644 --> 00:18:36,571\\n- We took the red-eye.\\n- Well, come on in.\\n\\n327\\n00:18:36,741 --> 00:18:38,089\\nThank you kindly.\\n\\n328\\n00:18:39,001 --> 00:18:41,425\\n- Can I get you something to drink?\\n- No, thank you.\\n\\n329\\n00:18:41,550 --> 00:18:44,917\\nIf y'all don't mind,\\nI got a hankerin' for a Lone Star beer.\\n\\n330\\n00:18:47,206 --> 00:18:50,498\\nThere's no alcohol in this household.\\nStop talking like that and lose the hat.\\n\\n331\\n00:18:53,386 --> 00:18:55,772\\nSorry. I'll take a diet Yoo-Hoo\\nif you have it.\\n\\n332\\n00:18:57,569 --> 00:18:59,019\\nYou'll take a cola.\\n\\n333\\n00:18:59,990 --> 00:19:01,971\\nWhat about you?\\nRadge, isn't it?\\n\\n334\\n00:19:02,500 --> 00:19:04,679\\nYou still having trouble\\ntalking to the ladies?\\n\\n335\\n00:19:05,567 --> 00:19:06,880\\nBecause, at our church,\\n\\n336\\n00:19:07,005 --> 00:19:09,224\\nwe have a woman\\nwho's an amazing healer.\\n\\n337\\n00:19:09,475 --> 00:19:12,050\\nMostly she does\\ncrutch and wheelchair people,\\n\\n338\\n00:19:12,532 --> 00:19:15,023\\nbut I bet she'd be willing\\nto take a shot\\n\\n339\\n00:19:15,148 --> 00:19:18,196\\nat whatever Third World demon\\nis running around inside of you.\\n\\n340\\n00:19:20,665 --> 00:19:21,691\\nIf you don't mind,\\n\\n341\\n00:19:21,816 --> 00:19:23,877\\nthere's a 3:05 nonstop\\nback to Los Angeles\\n\\n342\\n00:19:24,002 --> 00:19:26,621\\nand you have no idea\\nhow much I want to be on it.\\n\\n343\\n00:19:27,413 --> 00:19:29,579\\n- A girl?\\n- Uh, yes, ma'am.\\n\\n344\\n00:19:29,795 --> 00:19:31,403\\nOh, good.\\nI been praying for you.\\n\\n345\\n00:19:37,643 --> 00:19:39,867\\n- What are they doing here?\\n- We came to apologize.\\n\\n346\\n00:19:39,992 --> 00:19:41,580\\n- Again.\\n- And bring you home.\\n\\n347\\n00:19:41,705 --> 00:19:43,804\\nWhy don't you pack up your stuff\\nand we'll head back?\\n\\n348\\n00:19:45,375 --> 00:19:47,160\\nNo, this is my home now.\\n\\n349\\n00:19:47,579 --> 00:19:49,537\\nThanks to you, my career is over\\n\\n350\\n00:19:49,662 --> 00:19:51,894\\nand I will spend\\nthe rest of my life here in Texas\\n\\n351\\n00:19:52,019 --> 00:19:54,110\\ntrying to teach evolution\\nto creationists.\\n\\n352\\n00:19:58,143 --> 00:19:59,738\\nYou watch your mouth, Shelly.\\n\\n353\\n00:20:00,866 --> 00:20:02,657\\nEveryone's entitled\\nto their opinion.\\n\\n354\\n00:20:02,828 --> 00:20:05,326\\nEvolution isn't an opinion,\\nit's fact.\\n\\n355\\n00:20:05,581 --> 00:20:07,982\\nAnd that is your opinion.\\n\\n356\\n00:20:11,705 --> 00:20:13,619\\nI forgive you.\\nLet's go home.\\n\\n357\\n00:20:18,561 --> 00:20:20,499\\nDon't tell me prayer doesn't work.\\n\\n358\\n00:20:30,544 --> 00:20:33,124\\nHow about that?\\nI finally caught a break.\\n\\n359\\n00:20:39,240 --> 00:20:41,680\\nYou know how they say\\nwhen friends have sex,\\n\\n360\\n00:20:41,805 --> 00:20:43,155\\nit can get weird?\\n\\n361\\n00:20:43,489 --> 00:20:44,489\\nSure.\\n\\n362\\n00:20:45,926 --> 00:20:47,926\\nWhy does it have to get weird?\\n\\n363\\n00:20:48,679 --> 00:20:49,829\\nI don't know.\\n\\n364\\n00:20:50,553 --> 00:20:51,553\\nI mean,\\n\\n365\\n00:20:52,341 --> 00:20:55,031\\nwe were friends,\\nand now we're more than friends.\\n\\n366\\n00:20:55,615 --> 00:20:57,365\\nWe're whatever \"this\" is.\\n\\n367\\n00:20:57,625 --> 00:20:59,875\\nBut why label it, right?\\nI mean...\\n\\n368\\n00:21:00,725 --> 00:21:02,114\\nIt is what it is and...\\n\\n369\\n00:21:02,239 --> 00:21:04,439\\n- Leonard? It's weird.\\n- Totally.\\n\\n9999\\n00:00:0,500 --> 00:00:2,00\\n<font color=\"#ffff00\" size=14>www.tvsubtitles.net</font>\\n");
            Matcher matcher = pattern.matcher(s);

            while(matcher.find()) {
                String start = matcher.group(2) + ":" + matcher.group(3) + ":" + matcher.group(4) + "." + matcher.group(5);
                String end =  matcher.group(6) + ":" + matcher.group(7) + ":" + matcher.group(8) + "." + matcher.group(9);
                String subtitle = matcher.group(11);
                createFragmentAnnotation(connection,factory,uri,locators,subtitle,start,end);
            }

//            try {
//                contentService.setContentData(uri,s.getBytes(),"UTF-8");
//            } catch (WritingNotSupportedException e) {
//                throw new IOException(e.getMessage());
//            }

            connection.commit();

            return 1;
        } catch (Exception e) {
            log.error(e.getMessage());
            if(connection != null) {
                try {
                    connection.rollback();
                } catch (RepositoryException e1) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        } finally {
            if(connection != null) {
                try {
                    connection.close();
                } catch (RepositoryException e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
        return 0;
    }

    private void createFragmentAnnotation(RepositoryConnection connection, ValueFactory factory, URI resource, List<String> locators, String subtitle, String start, String end) throws RepositoryException {
        URI fragment = factory.createURI(configurationService.getBaseUri() + "resource/fragment/"+ UUID.randomUUID());//#t="+start+","+end);
        connection.add(resource, MA.hasFragment,fragment);
        for(String loc : locators) {
            connection.add(fragment,MA.locator,factory.createLiteral(loc+"#t="+start+","+end));
        }

        if(locators.isEmpty()) {
            //TODO if resource has no locator
            connection.add(fragment,factory.createURI("http://connectme.at/ontology#start"),factory.createLiteral(start));
            connection.add(fragment,factory.createURI("http://connectme.at/ontology#end"),factory.createLiteral(end));
        }

        connection.add(fragment, DC.DESCRIPTION, factory.createLiteral(subtitle));
        connection.add(fragment, RDF.TYPE, MA.MediaFragment);
    }
}
