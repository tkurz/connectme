package at.connectme.importer.model.mediarss;

import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

import java.util.HashSet;
import java.util.Locale;

/**
 * User: Thomas Kurz
 * Date: 06.08.12
 * Time: 11:58
 */
@RDFType(Namespace.MA.Collection)
public interface MediaChannelFacade extends Facade {

	@RDF(Namespace.MA.title)
	public String getTitle(Locale l);
	public void setTitle(String title, Locale l);

	@RDF(Namespace.MA.description)
	public String getDescription(Locale l);
	public void setDescription(String description, Locale l);

	@RDF(Namespace.MA.hasMember)
	public HashSet<VideoFacade> getVideos();
	public void setVideos(HashSet<VideoFacade> videos);

}
