package at.connectme.importer.model.mediarss;

import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

/**
 * Created with IntelliJ IDEA.
 * User: tkurz
 * Date: 27.11.12
 * Time: 11:04
 * To change this template use File | Settings | File Templates.
 */
@RDFType(Namespace.SKOS.Concept)
public interface SkosConcept extends Facade {

    @RDF(Namespace.SKOS.prefLabel)
    public String getTitle();
    public void setTitle(String title);

}
