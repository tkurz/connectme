package at.connectme.importer.model.mediarss;

/**
 * Created with IntelliJ IDEA.
 * User: tkurz
 * Date: 27.09.12
 * Time: 10:27
 * To change this template use File | Settings | File Templates.
 */

import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

@RDFType(Namespace.FOAF.Agent)
public interface AgentFacade extends Facade {

    @RDF(Namespace.RDFS.label)
    public String getLabel();
    public void setLabel(String label);
}
