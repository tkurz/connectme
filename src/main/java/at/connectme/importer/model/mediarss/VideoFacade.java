package at.connectme.importer.model.mediarss;

import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;
import org.apache.marmotta.commons.vocabulary.FOAF;
import org.openrdf.model.URI;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * User: Thomas Kurz
 * Date: 06.08.12
 * Time: 11:36
 */
@RDFType(Namespace.MA.MediaResource)
public interface VideoFacade extends Facade {

	@RDF(Namespace.MA.title)
	public String getTitle();
	public void setTitle(String title);

	@RDF(Namespace.MA.description)
	public String getDescription();
	public void setDescription(String description);

	@RDF(Namespace.MA.releaseDate)
	public Date getReleaseDate();
	public void setReleaseDate(Date releaseDate);

    @RDF(Namespace.MA.hasPublisher)
    public AgentFacade getPublisher();
    public void setPublisher(AgentFacade publisher);

	@RDF(Namespace.MA.hasKeyword)
	public HashSet<SkosConcept> getKeywords();
	public void setKeywords(HashSet<SkosConcept> keywords);

	@RDF(Namespace.MA.isMemberOf)
	public HashSet<MediaChannelFacade> getChannels();
	public void setChannels(HashSet<MediaChannelFacade> channels);

	@RDF(Namespace.MA.locator)
	public Set<String> getLocator();
	public void setLocator(Set<String> locator);

	@RDF(Namespace.FOAF.based_near)
	public PointFacade getFOAFLocation();
	public void setFOAFLocation(PointFacade location);

    @RDF(Namespace.MA.hasRelatedLocation)
    public URI getLocation();
    public void setLocation(URI location);

    @RDF(Namespace.MA.hasRelatedImage)
    public URI getThumbnail();
    public void setThumbnail(URI thumbnail);

    @RDF(Namespace.DCTERMS.identifier)
    public String getIdentifier();
    public void setIdentifier(String guid);

    @RDF(Namespace.FOAF.homepage)
    public URI getHomepage();
    public void setHomepage(URI homepage);

}
