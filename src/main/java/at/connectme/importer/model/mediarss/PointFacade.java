package at.connectme.importer.model.mediarss;

import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.annotations.RDF;
import org.apache.marmotta.commons.sesame.facading.annotations.RDFType;
import org.apache.marmotta.commons.sesame.facading.model.Facade;

/**
 * User: Thomas Kurz
 * Date: 06.08.12
 * Time: 13:58
 */
@RDFType(Namespace.GEO.Point)
public interface PointFacade extends Facade {

	@RDF(Namespace.GEO.lat)
	public double getLatitude();
	public void setLatitude(double latitude);

	@RDF(Namespace.GEO.long_)
	public double getLongitude();
	public void setLongitude(double longitude);

}
