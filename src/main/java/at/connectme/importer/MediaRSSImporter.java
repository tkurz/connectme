package at.connectme.importer;

import at.connectme.importer.model.mediarss.*;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.facading.api.Facading;
import org.apache.marmotta.commons.vocabulary.*;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.importer.Importer;
import org.apache.marmotta.platform.core.api.task.Task;
import org.apache.marmotta.platform.core.api.task.TaskManagerService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.exception.io.MarmottaImportException;
import org.openrdf.model.Resource;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.repository.RepositoryConnection;
import org.rometools.feed.module.georss.GeoRSSModule;
import org.rometools.feed.module.mediarss.MediaEntryModule;
import org.rometools.feed.module.mediarss.types.MediaContent;
import org.rometools.feed.module.mediarss.types.MediaGroup;
import org.slf4j.Logger;
import com.sun.syndication.io.XmlReader;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

/**
 * User: Thomas Kurz
 * TODO: implement
 * Date: 20.06.12
 * Time: 10:54
 */
@ApplicationScoped
public class MediaRSSImporter implements Importer {

    public static String BASE_URI_KEYWORD = "resource/";

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private TaskManagerService taskManagerService;

    @Inject
    private SesameService sesameService;

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss 'GMT'", Locale.US);

    private static final HashSet<String> types = new HashSet<String>(){{
        this.add("application/media+rss");
    }};

    @Override
    public String getName() {
        return "Media RSS Importer";
    }

    @Override
    public String getDescription() {
        return "This importer imports Media RSS into Marmotta";
    }

    @Override
    public Set<String> getAcceptTypes() {
        return types;
    }

    @Override
    public int importData(URL url, String format, Resource user, URI context) throws MarmottaImportException {
        int i = 0;
        BufferedReader r = null;
        try {
            URLConnection con = url.openConnection();
            con.setUseCaches(false);

            r = new BufferedReader (new InputStreamReader(con.getInputStream()));

            SyndFeed feed = new SyndFeedInput().build(r);
            i = importData(feed,user,context);
        } catch (IOException e) {
            throw new MarmottaImportException(e);
        } catch (FeedException e) {
            throw new MarmottaImportException(e);
        } finally {
            if (r != null)
                try {
                    r.close();
                } catch (IOException e) {
                    throw new MarmottaImportException(e.getMessage());
                }
        }
        return i;
    }

    @Override
    public int importData(InputStream is, String format, Resource user, URI context) throws MarmottaImportException {
        int i = 0;
        XmlReader reader = null;
        try {
            reader = new XmlReader(is);
            SyndFeed feed = new SyndFeedInput().build(reader);
            i = importData(feed,user,context);
        } catch (IOException e) {
            throw new MarmottaImportException(e);
        } catch (FeedException e) {
            throw new MarmottaImportException(e);
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new MarmottaImportException(e.getMessage());
                }
        }
        return i;
    }

    @Override
    public int importData(Reader reader, String format, Resource user, URI context) throws MarmottaImportException {
        int i = 0;
        try {
            SyndFeed feed = new SyndFeedInput().build(reader);
            i = importData(feed,user,context);
        } catch (FeedException e) {
            throw new MarmottaImportException(e);
        } finally {
            if (reader != null)
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new MarmottaImportException(e.getMessage());
                }
        }
        return i;
    }

    private int importData(SyndFeed feed, Resource user, URI context) throws MarmottaImportException {
        int nbOfVideos = 0;

        final String taskName = String.format("Media RSS Importer Task");
        Task task = taskManagerService.createSubTask(taskName,"Importer");
        task.updateMessage("importing data into Apache Marmotta repository");

        try {
            RepositoryConnection c_import = sesameService.getConnection();
            ValueFactory factory = c_import.getValueFactory();
            try {
                c_import.begin();

                URI channel = factory.createURI(feed.getLink());
                c_import.add(channel, RDF.TYPE, MA.Collection);
                c_import.add(channel, MA.title, factory.createLiteral(feed.getTitle()));
                c_import.add(channel, MA.description, factory.createLiteral(feed.getDescription().toString()));

                for(Iterator i = feed.getEntries().iterator(); i.hasNext();) {
                    SyndEntry entry = (SyndEntry) i.next();

                    URI item = factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"video/"+entry.getUri().toLowerCase().replaceAll(":","_"));

                    c_import.add(item,RDF.TYPE,MA.MediaResource);
                    c_import.add(item, FOAF.homepage,factory.createURI(entry.getLink()));
                    c_import.add(item,MA.title,factory.createLiteral(entry.getTitle()));
                    c_import.add(item,MA.description,factory.createLiteral(entry.getDescription().getValue()));
                    c_import.add(item,MA.releaseDate,factory.createLiteral(entry.getPublishedDate()));
                    c_import.add(item,DCTERMS.identifier,factory.createLiteral(entry.getUri().toLowerCase()));

                    URI agent = factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"media_publisher/"+URLEncoder.encode(entry.getAuthor(),"UTF-8"));
                    c_import.add(agent,RDF.TYPE,FOAF.Agent);
                    c_import.add(agent,RDFS.LABEL,factory.createLiteral(entry.getAuthor()));
                    c_import.add(item,MA.hasPublisher,agent);

                    //media group
                    MediaEntryModule media = (MediaEntryModule) entry.getModule(MediaEntryModule.URI);
                    MediaGroup group = media.getMediaGroups()[0];

                    for(MediaContent mc : group.getContents()) {
                        c_import.add(item,MA.locator,factory.createLiteral(mc.getReference().toString()));
                    }

                    String[] ks = group.getMetadata().getKeywords();
                    for(String k : ks) {
                        String kw = k.trim();
                        String kw_encoded = URLEncoder.encode(kw, "UTF-8");
                        URI concept = factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"concept/"+kw_encoded);
                        c_import.add(concept,RDF.TYPE, SKOS.Concept);
                        c_import.add(concept, SKOS.prefLabel, factory.createLiteral(kw));
                        c_import.add(item,MA.hasKeyword,concept);
                    }

                    //thumbnail
                    if (group.getMetadata().getThumbnail()[0] != null) {
                        c_import.add(item,MA.hasRelatedImage,factory.createLiteral(group.getMetadata().getThumbnail()[0].getUrl().toString()));
                    }

                    //location
                    GeoRSSModule geo = (GeoRSSModule) entry.getModule(GeoRSSModule.GEORSS_GEORSS_URI);
                    if(geo != null) {
                        String geoname = geo.getPosition().getLatitude() + "/" + geo.getPosition().getLongitude();
                        URI point = factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"point/"+geoname);
                        c_import.add(point,RDF.TYPE, GEO.Point);
                        c_import.add(point,GEO.lat,factory.createLiteral(geo.getPosition().getLatitude()));
                        c_import.add(point,GEO.long_,factory.createLiteral(geo.getPosition().getLongitude()));
                        c_import.add(item,FOAF.based_near,point);
                    }

                    //bind to channel
                    c_import.add(channel,MA.hasMember,item);
                    c_import.add(item,MA.isMemberOf,channel);

                    nbOfVideos++;
                }

                c_import.commit();

            } catch (Exception e) {
                log.error("Some exception during mediarss import: "+ e.getMessage());
                c_import.rollback();
                throw e;
            } finally {
                c_import.close();
            }
        } catch (Exception e) {
            throw new MarmottaImportException(e);
        } finally {
            taskManagerService.endTask(task);
        }

        return nbOfVideos;

    }

    //TODO THIS IMPL DID NOT WORK PROPErLY DUE TO FACADING
    /*
    private int importData(SyndFeed feed, Resource user, URI context) throws MarmottaImportException {
        int nbOfVideos = 0;

        final String taskName = String.format("Media RSS Importer Task");
        Task task = taskManagerService.createSubTask(taskName,"Importer");
        task.updateMessage("importing data into Apache Marmotta repository");

        try {
            RepositoryConnection c_import = sesameService.getConnection();

            Facading facading = FacadingFactory.createFacading(c_import);
            ValueFactory factory = c_import.getValueFactory();
            try {

                c_import.begin();

                MediaChannelFacade channel = facading.createFacade(factory.createURI(feed.getLink()),MediaChannelFacade.class, context);
                channel.setTitle(feed.getTitle(), new Locale(feed.getLanguage()));
                channel.setDescription(feed.getDescription().toString(),new Locale(feed.getLanguage()));

                HashSet<VideoFacade> videos = new HashSet<VideoFacade>();

                for(Iterator i = feed.getEntries().iterator(); i.hasNext();) {

                    SyndEntry entry = (SyndEntry) i.next();
                    URI item_uri = factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"video/"+entry.getUri().toLowerCase().replaceAll(":","_"));
                    VideoFacade item = facading.createFacade(item_uri,VideoFacade.class,context);

                    item.setHomepage(factory.createURI(entry.getLink()));

                    //basic video properties
                    item.setTitle(entry.getTitle());
                    item.setDescription(entry.getDescription().getValue());
                    item.setReleaseDate(entry.getPublishedDate());
                    item.setIdentifier(entry.getUri().toLowerCase());

                    //set agent
                    AgentFacade agent = facading.createFacade(factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"media_publisher/"+entry.getAuthor()),AgentFacade.class,context);
                    agent.setLabel(entry.getAuthor());
                    item.setPublisher(agent);

                    //media group
                    MediaEntryModule media = (MediaEntryModule) entry.getModule(MediaEntryModule.URI);
                    MediaGroup group = media.getMediaGroups()[0];

                    Set<String> vs = new HashSet<String>();
                    for(MediaContent mc : group.getContents()) {
                        String video = mc.getReference().toString();
                        vs.add(video);
                    }
                    item.setLocator(vs);

                    //keywords
                    HashSet<SkosConcept> set = new HashSet<SkosConcept>();
                    String[] ks = group.getMetadata().getKeywords();
                    for(String k : ks) {
                        String kw = k.trim();
                        String kw_encoded = URLEncoder.encode(kw, "UTF-8");
                        SkosConcept concept = facading.createFacade(factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"concept/"+kw_encoded),SkosConcept.class,context);
                        concept.setTitle(kw);
                        set.add(concept);
                    }
                    item.setKeywords(set);

                    //thumbnail
                    if (group.getMetadata().getThumbnail()[0] != null) {
                        URI thumb = factory.createURI(group.getMetadata().getThumbnail()[0].getUrl().toString());
                        item.setThumbnail(thumb);
                    }

                    //location
                    GeoRSSModule geo = (GeoRSSModule) entry.getModule(GeoRSSModule.GEORSS_GEORSS_URI);
                    if(geo != null) {
                        String geoname = geo.getPosition().getLatitude() + "/" + geo.getPosition().getLongitude();
                        PointFacade point = facading.createFacade(factory.createURI(configurationService.getBaseUri()+BASE_URI_KEYWORD+"point/"+geoname),PointFacade.class,context);
                        point.setLatitude(geo.getPosition().getLatitude());
                        point.setLongitude(geo.getPosition().getLongitude());
                        item.setFOAFLocation(point);
                    }

                    //bind to channel
                    videos.add(item);
                    HashSet<MediaChannelFacade> channels = item.getChannels()!=null ? item.getChannels() : new HashSet<MediaChannelFacade>();
                    channels.add(channel);

                    item.setChannels(channels);

                    nbOfVideos++;
                }

                channel.setVideos(videos);
                c_import.commit();

            } catch (Exception e) {
                log.error("Some exception during mediarss import: "+ e.getMessage());
                c_import.rollback();
                throw e;
            } finally {
                c_import.close();
            }
        } catch (Exception e) {
            throw new MarmottaImportException(e);
        } finally {
            taskManagerService.endTask(task);
        }

        return nbOfVideos;
    }
    */
}