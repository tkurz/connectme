package at.connectme.webservice;

import at.connectme.importer.MediaRSSImporter;
import at.connectme.importer.SRTImporter;
import at.connectme.importer.model.mediarss.SkosConcept;
import at.connectme.importer.model.mediarss.VideoFacade;
import org.apache.marmotta.commons.constants.Namespace;
import org.apache.marmotta.commons.sesame.facading.FacadingFactory;
import org.apache.marmotta.commons.sesame.facading.api.Facading;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.api.user.UserService;
import org.apache.marmotta.platform.ldpath.api.LDPathService;
import org.apache.marmotta.platform.sparql.api.sparql.SparqlService;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.query.QueryLanguage;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: tkurz
 * Date: 17.10.12
 * Time: 13:49
 * To change this template use File | Settings | File Templates.
 */
@ApplicationScoped
@Path("/video/resource")
public class VideoResourceWebservice {

    private static String basicYoovisUrl = "http://yoovis.tv/connectme/mrss_videolistchannel.aspx?pw=connect";

    @Inject
    MediaRSSImporter mediaRSSImporter;

    @Inject
    SRTImporter srtImporter;

    @Inject
    private UserService userService;

    @Inject
    private ContextService contextService;

    @Inject
    private SesameService sesameService;

    @Inject
    private LDPathService ldPathService;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private SparqlService sparqlService;

    private static HashMap<String,URI> contexts;

    static {
        contexts = new HashMap<String, URI>();
        ValueFactory factory = ValueFactoryImpl.getInstance();
        contexts.put("yoovis",factory.createURI("http://yoovis.tv/resource/context"));
        contexts.put("psmedia",factory.createURI("http://psmedia.tv/resource/context"));
    }

    /**
     * return a json object that includes the title and url of the video and the urls and types of all locators. If origin is set,
     * the system tries to import these information. If not, a resource with 'url' as locator is created and returned.
     * @param id
     * @param origin
     * @param channel
     * @return
     */
    @GET
    @Path("/rss")
    @Produces("application/json")
    public Response getInfoFromRss(@QueryParam("id")String id,@QueryParam("origin")String origin,@QueryParam("channel")String channel) {
        if(origin != null) {
            try {
                if(channel == null || id == null ) throw new Exception("channel and id may not be undefined");
                //import data
                mediaRSSImporter.importData(buildRssUrl(origin,channel,id),"application/media+rss",userService.getCurrentUser(),getContext(origin));
                //build response
                List<Map<String,Value>> result = sparqlService.query(QueryLanguage.SPARQL,"SELECT ?url WHERE {?url <"+ Namespace.DCTERMS.identifier+"> \""+getIdentifier(origin,id)+"\"}");
                if(result.size() != 0) {
                    return Response.ok().entity(buildInfo((URI) result.get(0).get("url"),null,null,null,null)).build();
                } else throw new Exception("cannot create resource");

            } catch (Exception e) {
                return Response.status(404).entity("cannot get resource from '"+origin+"': "+e.getMessage()).build();
            }

        } else return Response.status(404).entity("sourcetype '"+origin+"' is not defined").build();
    }

    /**
     * Allows the import of srt file resources for a given video url. To try it with curl: curl -X POST --data-binary @tbbt.sample.en.sr --data-binary "@tbbt.sample.en.srt" "http://localhost:8080/video/resource/srt?title=Hello+World&url=http://ecample.org/video/123"
     * @param url the video
     * @param title the tile, may be null
     * @param request for internal usage
     * @return
     */
    @POST
    @Path("/srt")
    @Produces("application/json")
    public Response getInfoFromRss(@QueryParam("url")String url,@QueryParam("title")String title, @Context HttpServletRequest request) {
        if(url != null)
            try {
                ValueFactory factory = ValueFactoryImpl.getInstance();
                URI uri = factory.createURI(getUrl(url));

                List<String> locators = new ArrayList<>();
                locators.add(url);

                srtImporter.importData(request.getInputStream(),"application/x-subrip",userService.getCurrentUser(),uri,locators);
                return Response.ok().entity(buildInfo(url, title, null, null,null)).build();
            } catch(Exception e) {
                e.printStackTrace();
                return Response.serverError().entity("could not get data").build();
            }
        else return Response.status(404).entity("url must be defined").build();
    }

    /**
     * return a json object that includes the title and url of the video and the urls and types of all locators. The method sets
     * keyword and description, too (if given).
     * @param url
     * @param title
     * @param description
     * @param keywords
     * @return
     */
    @GET
    @Produces("application/json")
    @Path("/url")
    public Response getInfoFromURL(@QueryParam("url")String url,@QueryParam("title")String title,@QueryParam("description")String description,@QueryParam("keywords")String keywords,@QueryParam("location")String location) {
        if(url != null)
            try {
                return Response.ok().entity(buildInfo(url,title,description,keywords,location)).build();
            } catch(Exception e) {
                e.printStackTrace();
                return Response.serverError().entity("could not get data").build();
            }
        else return Response.status(404).entity("url must be defined").build();
    }

    private URL buildRssUrl(String origin, String channel, String id) throws Exception {
        if(origin.equals("yoovis")) {
            return new URL(basicYoovisUrl+"&channel="+channel+"&videoid="+id);
        } else throw new Exception("origin is not supported");
    }

    private String getIdentifier(String origin, String id) throws Exception {
        if(origin.equals("yoovis")) {
            return origin+"id:"+id;
        } else throw new Exception("origin is not supported");
    }

    private URI getContext(String origin) {
        if(contexts.containsKey(origin)) return contexts.get(origin);
        else return contextService.getDefaultContext();
    }

    /**
     * Allows to add an accepted video format (e.g. ogv)
     * @param format
     * @return
     */
    @POST
    @Path("/format/{format}")
    public Response addFormat(@PathParam("format")String format) {
        if(format.equals("")) return Response.status(Response.Status.BAD_REQUEST).build();
        suffixes.add("."+format);
        return Response.ok().build();
    }

    private static Set<String> suffixes = new HashSet<String>(){{
        add(".mp4");
        add(".webm");
        add(".ogv");
        add(".ogg");
    }};

    private String getUrl(String url) {
        String format = url.substring(url.lastIndexOf("."));
        if(suffixes.contains(format)) {
            return url.substring(0,url.length()-format.length());
        }
        else return url;
    }

    private Info buildInfo(String url, String title, String description, String keywords, String location) throws Exception {
        try {
            RepositoryConnection c_import = sesameService.getConnection();
            Facading facading = FacadingFactory.createFacading(c_import);
            try {
                c_import.begin();
                //get video
                VideoFacade video = facading.createFacade(getUrl(url),VideoFacade.class);
                //add locator to resource
                Set<String> set = video.getLocator() == null ? new HashSet<String>() : video.getLocator();
                set.add(url);
                video.setLocator(set);

                setProperties(facading,video,title,description,keywords,location);

                Info info = buildInfo(video);

                c_import.commit();

                return info;
            } finally {
                c_import.close();
            }
        } catch (RepositoryException e) {
            throw new Exception("cannot create video resource");
        }
    }

    private void setProperties(Facading facading, VideoFacade video, String title, String description, String keywords,String location) throws UnsupportedEncodingException {
        if(title != null) video.setTitle(title);
        if(description != null) video.setDescription(description);
        if(location != null) {
            ValueFactory factory = ValueFactoryImpl.getInstance();
            video.setLocation(factory.createURI(location));
        }
        if(keywords != null) {
            HashSet<SkosConcept> set = new HashSet<SkosConcept>();
            String[] ks = keywords.split(",");
            for(String k : ks) {
                String kw = k.trim();
                String kw_encoded = URLEncoder.encode(kw, "UTF-8");
                SkosConcept concept = facading.createFacade(configurationService.getBaseUri()+MediaRSSImporter.BASE_URI_KEYWORD+"concept/"+kw_encoded,SkosConcept.class);
                concept.setTitle(kw);
                set.add(concept);
            }
            video.setKeywords(set);
        }
    }

    private Info buildInfo(URI video_uri, String title, String description, String keywords,String location) throws Exception {
        try {
            RepositoryConnection c_import = sesameService.getConnection();
            Facading facading = FacadingFactory.createFacading(c_import);
            try {
                c_import.begin();
                //get video
                VideoFacade video = facading.createFacade(video_uri,VideoFacade.class);

                setProperties(facading,video,title,description,keywords,location);

                Info info = buildInfo(video);

                c_import.commit();

                return info;
            } finally {
                c_import.close();
            }
        } catch (RepositoryException e) {
            throw new Exception("cannot build result");
        }
    }

    private Info buildInfo(VideoFacade video) {
        Info info = new Info();
        info.setTitle(video.getTitle());
        info.setUrl(video.getDelegate().toString());
        for(String source : video.getLocator()) {
            info.getSources().add(source);
        }
        return info;
    }

    private class Info {

        String title;
        String url;
        List<String> sources;

        public Info() {
            sources = new ArrayList<String>();
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public List<String> getSources() {
            return sources;
        }

        public void setSources(List<String> sources) {
            this.sources = sources;
        }

    }

}
