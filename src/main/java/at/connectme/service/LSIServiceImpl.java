package at.connectme.service;

import at.connectme.api.LSIService;
import at.connectme.api.LSIServiceException;
import at.connectme.api.MediaType;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.api.importer.ImportService;
import org.apache.marmotta.platform.core.api.triplestore.ContextService;
import org.apache.marmotta.platform.core.api.triplestore.SesameService;
import org.apache.marmotta.platform.core.api.user.UserService;
import org.openrdf.model.URI;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.sail.memory.MemoryStore;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.lang.Exception;import java.lang.Override;import java.lang.String;import java.net.MalformedURLException;

/**
 * For documentation of the LSI webservice ,see http://new.devserver.sti2.org/lsi/documentation.html
 * User: tkurz
 * Date: 10.10.12
 * Time: 11:17
 * To change this template use File | Settings | File Templates.
 */
@ApplicationScoped
public class LSIServiceImpl implements LSIService {

    @Inject
    private Logger log;

    @Inject
    private ConfigurationService configurationService;

    @Inject
    private ContextService contextService;

    @Inject
    private ImportService importService;

    @Inject
    private SesameService sesameService;

    @Inject
    private UserService userService;

    private String LSIDefaultServiceURL = "http://new.devserver.sti2.org/lsi/api/invoke";
    private int LSIDefaultLimit = 10;
    private MediaType LSIDefaultMediaType = MediaType.all;

    private String knowledgeSpaceUrl = null;

    @PostConstruct
    public void initialise() {
        knowledgeSpaceUrl = configurationService.getBaseUri() + "context/lsi";
    }

    @Override
    public void run(URI resource, URI context, MediaType mediaType, int limit, boolean ner, Repository repository ) throws LSIServiceException {
        HttpClient client = new HttpClient();
        try {
            GetMethod method = new GetMethod(buildRequestUrl(resource,context,mediaType,limit,ner));

            method.getParams().setParameter(HttpMethodParams.SO_TIMEOUT,30000);

            if(client.executeMethod(method) == 200) {

                RepositoryConnection connection = repository.getConnection();
                connection.begin();
                try {
                    //parse results
                    connection.add(method.getResponseBodyAsStream(), "", RDFFormat.forMIMEType(method.getResponseHeader("Content-Type").getValue()));
                    connection.commit();
                } catch (Exception e) {
                    connection.rollback();
                    throw new Exception("Exception while importing LSI results");
                } finally {
                    connection.close();
                }

            } else throw new Exception("Exception while executing sti service call");

        } catch (Exception e) {
            throw new LSIServiceException(e.getMessage());
        }
    }

    private String buildRequestUrl(URI resource, URI context, MediaType mediaType, int limit, boolean ner) throws MalformedURLException {

        if(limit==-1) limit = LSIDefaultLimit;
        if(mediaType == null) mediaType = LSIDefaultMediaType;

        String contextString="";
        if(context!=null) contextString = "&context="+context.toString();

        String limitString="&limit=" + limit;
        String mediaTypeString = "&mediaType="+mediaType;
        String nerString = "&ner="+ner;

        String lsiServiceURL = configurationService.getStringConfiguration("lsi.serviceUrl",LSIDefaultServiceURL);   //TODO URL encode
        String result =  lsiServiceURL + "?lod=" + resource + mediaTypeString + limitString + contextString + nerString;

        log.info("LSI QUERY:"+result);

        return result;
    }
}
