package at.connectme.api;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public class LSIServiceException extends Exception {

    public LSIServiceException() {
        super();
    }

    public LSIServiceException(String s) {
        super(s);
    }

}
