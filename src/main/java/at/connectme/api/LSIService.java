package at.connectme.api;

import org.openrdf.model.URI;
import org.openrdf.repository.Repository;

import java.util.Set;

/**
 * ...
 * <p/>
 * Author: Thomas Kurz (tkurz@apache.org)
 */
public interface LSIService {

    public void run(URI resource, URI context, MediaType mediaType, int limit, boolean ner, Repository repo) throws LSIServiceException;

}
